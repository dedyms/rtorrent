FROM registry.gitlab.com/dedyms/buster-slim:latest as tukang

# building deps
ARG DEPS="libcurl4-gnutls-dev zlib1g-dev libtool automake build-essential libncurses-dev libsigc++-2.0-dev libssl-dev libxmlrpc-c++8-dev git ca-certificates"
RUN apt update && apt install -y --no-install-recommends $DEPS && apt clean

# Build libtorrent
RUN git clone -b master https://github.com/rakshasa/libtorrent.git /libtorrent
WORKDIR /libtorrent
RUN sh autogen.sh && ./configure && make -j4 && make install

# Change to user build rtorrent
RUN git clone -b master https://github.com/rakshasa/rtorrent.git /rtorrent
WORKDIR /rtorrent
RUN sh autogen.sh && ./configure --with-xmlrpc-c && make -j4 && make install

# Copy out libtorrent lib and rtorrent
FROM registry.gitlab.com/dedyms/buster-slim:latest
RUN apt update && apt install -y --no-install-recommends libcurl3-gnutls ca-certificates libxmlrpc-core-c3 && apt clean && rm -rf /var/lib/apt/lists/*
COPY --from=tukang /usr/local/bin/ /usr/local/bin/
COPY --from=tukang /usr/local/lib/ /usr/local/lib
COPY entrypoint.sh /entrypoint.sh

# Install apache
ARG WEB="php-fpm php-json php-pdo php-mysql php-zip php-gd php-mbstring php-curl php-xml php-pear php-bcmath libapache2-mod-php libapache2-mod-scgi apache2"
ARG RUTORRENTNEED="curl ffmpeg python python-pip unzip unrar-free git sox mediainfo"
RUN apt update && apt install -y --no-install-recommends $WEB $RUTORRENTNEED && apt clean && rm -rf /var/lib/apt/lists/* && rm  /var/www/html/index.html
COPY .rtorrent.rc /home/$CONTAINERUSER/.rtorrent.rc
COPY ports.conf /etc/apache2/ports.conf
COPY envvars /etc/apache2/envvars
ENV PATH="/home/$CONTAINERUSER/.local/:$PATH"
# Global write torrent folder
RUN mkdir -p /torrent && chmod -R 777 /torrent
WORKDIR /torrent
RUN chown -R $CONTAINERUSER:$CONTAINERUSER /var/www/html/
USER $CONTAINERUSER
RUN pip install --no-cache --user cloudscraper
RUN git clone https://github.com/Novik/ruTorrent.git /var/www/html/ && mkdir -p /var/www/html/profile/torrents/
COPY --chown=$CONTAINERUSER:$CONTAINERUSER config.php /var/www/html/conf/config.php
ENTRYPOINT ["/entrypoint.sh"]
